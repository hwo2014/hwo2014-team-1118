package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods._
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.util.{Try, Success, Failure}

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

trait Piece;
case class Straight(length: Double, switch: Boolean = false) extends Piece;
case class Round(radius: Int, angle: Double) extends Piece;

case class CarId(name: String, color: String)

case class Lane(startLaneIndex: Int, endLaneIndex: Int)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: Lane, lap: Int)
case class Position(angle: Double, piecePos: PiecePosition)

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  var pieces = List[Piece]()
  var ownCarId = CarId("", "")
  send(MsgWrapper("join", Join(botName, botKey)))
  play

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      var throttle = 0.5
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("gameInit", json) =>
          println(json)
          println(compact(render(json)))
          val jsonPieces = json \ "race" \ "track" \ "pieces"
          val p = for {
            JArray(arr) <- jsonPieces
            piece <- arr
          } yield {
            Try(piece.extract[Straight]) match {
              case Success(sc) => sc
              case Failure(_) => piece.extract[Round]
            }
          }
          pieces = p
          println(p)
        case MsgWrapper("yourCar", json) =>
          ownCarId = json.extract[CarId]
        case MsgWrapper("carPositions", json) =>
          for {
            JArray(pos) <- json
            carPos <- pos
            carId = (carPos \ "id").extract[CarId] if carId == ownCarId
          } {
            println(carPos)
            val piecePos = (carPos \ "piecePosition").extract[PiecePosition]
            pieces(piecePos.pieceIndex) match {
              case Round(_, _) => throttle = 0.6
              case _ => throttle = 0.8
            }
            if (Math.abs((carPos \ "angle").extract[Double]) > 15) {
              throttle *= 0.0
            }
          }
        case MsgWrapper(msgType, rest) =>
          println("Received: " + msgType)
          println(rest)
      }

      send(MsgWrapper("throttle", throttle))
      play
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
